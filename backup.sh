
## #######################

## Database Name
database_name="$1"
backup_public_key="/home/arpit-legend/.ssh/backup_key.pem.pub"

## Location to place backups.
backup_dir="db_backups/"
## Numbers of days you want to keep copies of your databases

number_of_days=10
if [ -z "${database_name}" ]
then
 echo "Please specify a database name as the first argument"
 exit 1
fi

## String to append to the name of the backup files
backup_date=`date +%Y-%m-%d-%H-%M-%S`
backup_name="${database_name}.${backup_date}.sql.bz2.enc"
echo "Dumping ${database_name} to ${backup_dir}${backup_name}"

pg_dump -U arpit ${database_name}|bzip2|openssl smime -encrypt \
 -aes256 -binary -outform DEM \
 -out ${backup_dir}${backup_name} \
 "${backup_public_key}"

find ${backup_dir} -type f -prune -mtime \
    +${number_of_days} -exec rm -f {} \;

## push file to S3
aws s3 cp ${backup_dir}${backup_name} s3://mybucket355/db_backup/
